# Application Blueprint for WordPress

---

## What is WordPress?

WordPress is a open-source content-management system written in PHP, commonly used to create websites and blogs. WordPress is extremely customizable, with a large community of plugins and themes to design your site just the way you want it!

WordPress is extremely popular, and used by over 40% of the top 10 million websites.

**Official Website:** <https://wordpress.org>

This application blueprint uses the [`bitnami/wordpress`](https://hub.docker.com/r/bitnami/wordpress) Docker image.

## Filling Out a Deployment Blueprint

For the general steps to deploy an application with Unfurl Cloud, see our [Getting Started guides](https://unfurl.cloud/help#guides).

### Details

<!-- TODO: this could be better -->

To simplify the deployment process, we have set default values for several required inputs and hidden them from the UI. The remaining inputs are exposed under "Details", and must be filled in:

| Input Name                    | Default Value    |
|-------------------------------|------------------|
| WordPress Admin Username      | `wordpress`      |
| WordPress Admin Password      | (auto-generated) |
| WordPress Admin Email         | (none)           |
| WordPress Version             | `5.9.3`          |
| Database Password             | (auto-generated) |
| Subdomain *(see [DNS](#dns))* | `www`            |

Below is a list of the hidden inputs and their default values:

| Input Name       | Default Value |
|------------------|---------------|
| Database User    | `wordpress`   |
| Database Name    | `wordpress`   |
| Database Type    | `mysql`       |
| Database Version | `8.0`         |

### Components

Each application blueprint includes **components**. These are the required and optional resources for the application. In most cases, there is more than one way to fulfill a component requirement. Common components include Compute, DNS, Database, and Mail Server:

After selecting a deployment blueprint, you will be prompted to fulfill the requirements and configure the deployment to your needs.

#### Compute

WordPress requires the following compute VM resources:

- At least 1 CPU
- At least 1024 MB (1 GB) of RAM
- At least 10 GB of hard disk space

### DNS

A DNS provider must be specified for the deployed site to be accessible via a domain name.

Supported DNS providers are:

- Google Cloud DNS
- DigitalOcean DNS
- AWS Route53
- Azure DNS

All providers require a domain name to use. ***Note:** the domain must be registered to that service.*

The `Subdomain` input above will be used to register a new subdomain under the given domain. For example, given the subdomain `wp-example` and domain zone `mysite.com`, the site will be accessible at `wp-example.mysite.com`.

### Database: MySQL or MariaDB

WordPress can use either of these databases:

| Database | Supported Versions |
|----------|--------------------|
| MySQL    | `5.7`, `8.0`       |
| MariaDB  | `10.3` to `10.9`   |

### "Extra" Components

Optional blueprint components can be found under the "Extras" tab when filling out the blueprint. These components can offer extra features or enhancements to the site, but are not required for a basic deployment.

#### Mail Server

WordPress optionally requires a mail server to send out emails.

Two mail providers are supported, with their needed inputs detailed below:

<!-- use <br> for multi-line tables, GFM does not support multi-line tables -->

**SendGrid:**

| Input   | Description                                                                                    |
|---------|------------------------------------------------------------------------------------------------|
| API Key | This can be created at [SendGrid account settings](https://app.sendgrid.com/settings/api_keys) |

**Generic SMTP Server:**

| Input     | Description                                                                     |
|-----------|---------------------------------------------------------------------------------|
| SMTP Host | Name of the mail server, e.g. `smtp.someprovider.com`                           |
| User Name | Usually the email to send as. Double check the documentation for your provider! |
| Secret    | The password or login token to authenticate with.                               |
| Protocol  | Either `ssl` or `tls`, probably `tls`.                                          |

**Gmail:**

Use Generic SMTP Server with the following inputs:

| Input     | Value                                       |
|-----------|---------------------------------------------|
| SMTP Host | `smtp.gmail.com`                            |
| User Name | Gmail username (email without `@gmail.com`) |
| Secret    | Gmail password                              |
| Protocol  | `tls`                                       |
